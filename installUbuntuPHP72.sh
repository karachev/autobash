#!/bin/bash
clear

echo "Пароль администратора для PMA: "
read passAdmin

# general
apt-get update -y
apt-get upgrade -y
apt-get install ntpdate
cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime
ntpdate ru.pool.ntp.org
iptables -A INPUT -p tcp --match multiport --dports 22,80,443,8080 -j ACCEPT
echo "0 0 * * * root /usr/sbin/ntpdate ru.pool.ntp.org" > /etc/cron.d/timeSinc
apt-get install --allow-unauthenticated -y mc
apt-get install --allow-unauthenticated -y unzip
apt-get install --allow-unauthenticated -y software-properties-common
add-apt-repository universe
add-apt-repository ppa:certbot/certbot -y
apt-get update

# Nginx
apt-get install --allow-unauthenticated -y nginx
cp auto/t/i/nginx.conf /etc/nginx/nginx.conf
systemctl enable nginx
systemctl start nginx

# PHP
apt-get install --allow-unauthenticated -y php7.2 php7.2-cgi php7.2-cli php7.2-common php7.2-curl php7.2-dev php7.2-gd php7.2-gmp php7.2-json php7.2-ldap php7.2-mysql php7.2-odbc php7.2-opcache php7.2-pspell php7.2-readline php7.2-recode php7.2-snmp php7.2-sqlite3 php7.2-tidy php7.2-xml php7.2-xmlrpc php7.2-bcmath php7.2-bz2 php7.2-dba php7.2-enchant php7.2-fpm php7.2-imap php7.2-interbase php7.2-intl php7.2-mbstring php7.2-phpdbg php7.2-soap php7.2-sybase php7.2-xsl php7.2-zip php-mysql php-mysqli
systemctl enable php7.2-fpm
systemctl start php7.2-fpm

# IonCube
wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
tar xfz ioncube_loaders_lin_x86-64.tar.gz
mv ioncube /usr/local/
rm ioncube_loaders_lin_x86-64.tar.gz

# Nginx + PHP
cp auto/t/i/default /etc/nginx/sites-enabled/default
cp auto/t/i/www.conf /etc/php/7.2/fpm/pool.d/www.conf
systemctl restart nginx
systemctl restart php7.2-fpm
#echo '<?php phpinfo(); ?>' > /var/www/html/index.php

#SQL
apt-get install --allow-unauthenticated -y mariadb-server
systemctl enable mariadb
systemctl start mariadb
passRoot=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c20)
echo "root - $passRoot" >> dbinfo.txt
mysql -e "USE mysql; update user set password=PASSWORD('$passRoot') where User='root'"

#PHP + SQL
apt-get install --allow-unauthenticated -y php-mysql php-mysqli
systemctl restart php7.2-fpm

#PMA
apt-get install  --allow-unauthenticated -y phpmyadmin
cp auto/t/i/phpmyadmin.conf /etc/nginx/sites-enabled/phpmyadmin.conf
systemctl reload nginx
mysql -e "CREATE USER 'admin'@'localhost' IDENTIFIED BY '$passAdmin';"
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;"
mysql -e "FLUSH PRIVILEGES;"


#Memcached
apt-get install --allow-unauthenticated -y memcached php-memcached
systemctl enable memcached
systemctl start memcached
systemctl restart php7.2-fpm

# Apache
apt-get install --allow-unauthenticated -y apache2 libapache2-mod-php
cp auto/t/i/ports.conf /etc/apache2/ports.conf
cp auto/t/i/dir.conf /etc/apache2/mods-available/dir.conf
cp auto/t/i/apache2.conf /etc/apache2/apache2.conf
a2dismod mpm_event
a2enmod mpm_prefork
a2enmod php7.2
a2enmod setenvif
a2enmod rewrite
systemctl enable apache2
systemctl start apache2

#Apache Real IP
cp auto/t/i/remoteip.conf /etc/apache2/mods-available/remoteip.conf
a2enmod remoteip
systemctl restart apache2

#Postfix

#Config PHP
cp auto/t/i/apache2php.ini /etc/php/7.2/apache2/php.ini
cp auto/t/i/fpmphp.ini /etc/php/7.2/fpm/php.ini
echo 'zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.2.so' >> /etc/php/7.2/fpm/php.ini
echo 'zend_extension_ts = /usr/local/ioncube/ioncube_loader_lin_7.2_ts.so' >> /etc/php/7.2/fpm/php.ini
echo 'zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.2.so' >> /etc/php/7.2/apache2/php.ini
echo 'zend_extension_ts = /usr/local/ioncube/ioncube_loader_lin_7.2_ts.so' >> /etc/php/7.2/apache2/php.ini

systemctl restart php7.2-fpm
systemctl restart nginx
systemctl restart apache2

#Certbot
apt-get install --allow-unauthenticated -y certbot python3-certbot-nginx