#!/bin/bash
clear
generalLink="https://bitbucket.org/karachev/centos/raw/f95eeab7264296c58ed85842982ceb06e0abf292"
PS3="Настройка хостинга nginx+php-fpm. ВАЖНО! для групповых операций рядом должен лежать список в файле domains.txt! Что делаем: "
options=("Создать один хостинг домен" "Создать хостинг домены по списку" "Создать один домен под копирку" "Создать домены под копирку по списку" "Скопировать файл во все домены" "Скопировать файл в домены по списку" "Исправить коневой домен" "Удалить один домен" "Удалить домены по списку" "Удалить файл с доменов по списку" "Удалить все домены" "Выход")
select opt in "${options[@]}"
do
    case $opt in
        "Создать один хостинг домен")
            echo "Введите домен: "
            read domain
            mkdir -p /home/www/$domain
            chmod 777 -R /home/www
            chmod 777 /home/www/$domain
            chown -R www-data:www-data /home/www/$domain
            wget $generalLink/d/nginx_host -O /etc/nginx/conf.d/$domain.conf
            domain1="$(idn2 $domain)"
            sed -i 's/myvhost1/'$domain1'/g' /etc/nginx/conf.d/$domain.conf
            sed -i 's/myvhost/'$domain'/g' /etc/nginx/conf.d/$domain.conf
            echo $domain complete
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            break
            ;;
        "Создать хостинг домены по списку")
            echo "Введите хеш: "
            read hahash
            rm -f domains.txt
            wget http://pastebin.com/raw/$hahash -O domains.txt
            /usr/bin/perl   -pi -e 's/\r\n/\n/g' domains.txt
            echo "" >> domains.txt
            FILE=domains.txt
            wget $generalLink/d/nginx_host
            while read line; do
            mkdir -p /home/www/$line
            chown -R www-data:www-data /home/www/$line
            cp nginx_host /etc/nginx/conf.d/$line.conf
            line1="$(idn2 $line)"
            sed -i 's/myvhost1/'$line1'/g' /etc/nginx/conf.d/$line.conf
            sed -i 's/myvhost/'$line'/g' /etc/nginx/conf.d/$line.conf
            echo $line complete
            done < $FILE
            rm -f nginx_host
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            rm -f domains.txt
            break
            ;;
        "Создать один домен под копирку")
            echo "Введите домен: "
            read domain
            mkdir -p /home/www/$domain
            chmod 777 -R /home/www
            chmod 777 /home/www/$domain
            chown -R www-data:www-data /home/www/$domain
            wget $generalLink/d/nginx_copy_host -O /etc/nginx/conf.d/$domain.conf
            domain1="$(idn2 $domain)"
            sed -i 's/myvhost1/'$domain1'/g' /etc/nginx/conf.d/$domain.conf
            sed -i 's/myvhost/'$domain'/g' /etc/nginx/conf.d/$domain.conf
            echo $domain complete
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            break
            ;;
        "Создать домены под копирку по списку")
            echo "Введите хеш: "
            read hahash
            rm -f domains.txt
            wget http://pastebin.com/raw/$hahash -O domains.txt
            /usr/bin/perl   -pi -e 's/\r\n/\n/g' domains.txt
            echo "" >> domains.txt
            FILE=domains.txt
            wget $generalLink/d/nginx_copy_host
            while read line; do
            mkdir -p /home/www/$line
            chmod 777 -R /home/www
            chmod 777 /home/www/$domain
            chown -R www-data:www-data /home/www/$line
            cp nginx_copy_host /etc/nginx/conf.d/$line.conf
            line1="$(idn2 $line)"
            sed -i 's/myvhost1/'$line1'/g' /etc/nginx/conf.d/$line.conf
            sed -i 's/myvhost/'$line'/g' /etc/nginx/conf.d/$line.conf
            echo $line complete
            done < $FILE
            rm -f nginx_copy_host
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            rm -f domains.txt
            break
            ;;
        "Скопировать файл во все домены")
            echo "Имя файла: "
            read filename
            ls /home/www/ >> temp.txt
            FILE=temp.txt
            while read line; do
            cp -f $filename /home/www/$line
            chmod 777 /home/www/$line
            echo $line complete
            done < $FILE
            rm temp.txt
            break
            ;;
        "Скопировать файл в домены по списку")
            echo "Введите хеш: "
            read hahash
            rm -f domains.txt
            wget http://pastebin.com/raw/$hahash -O domains.txt
            /usr/bin/perl   -pi -e 's/\r\n/\n/g' domains.txt
            echo "" >> domains.txt
            FILE=domains.txt
            echo "Имя файла: "
            read filename
            while read line; do
            cp -f $filename /home/www/$line
            chmod 777 /home/www/$line          
            echo $line complete
            done < $FILE
            rm -f domains.txt
            break
            ;;
         "Исправить коневой домен")
            #ip="$(ifconfig | grep -A 1 'enp4s0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
            ip="$(ifconfig | grep -A 1 'enp4s0' | tail -1 | cut -d " " -f 10)"
            wget http://cdn.repo.cloudns.cc/s/nginx/root_domain -O /etc/nginx/conf.d/$ip.conf
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            break
            ;;
        "Удалить один домен")
            echo "Введите домен: "
            read domain
            rm -rf /home/www/$domain/*
            rm -f /etc/nginx/conf.d/$domain.conf
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            break
            ;;
    "Удалить домены по списку")
            echo "Введите хеш: "
            read hahash
            rm -f domains.txt
            wget http://pastebin.com/raw/$hahash -O domains.txt
            /usr/bin/perl   -pi -e 's/\r\n/\n/g' domains.txt
            echo "" >> domains.txt
            FILE=domains.txt
            while read line; do
            rm -rf /home/www/$line
            rm -f /etc/nginx/conf.d/$line.conf
            echo $line complete
            done < $FILE
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            rm -f domains.txt
            break
            ;;
        "Удалить файл с доменов по списку")
            echo "Введите хеш: "
            read hahash
            rm -f domains.txt
            wget http://pastebin.com/raw/$hahash -O domains.txt
            /usr/bin/perl   -pi -e 's/\r\n/\n/g' domains.txt
            echo "" >> domains.txt
            FILE=domains.txt
            echo "Имя файла: "
            read filename
            while read line; do
            rm -f /home/www/$line/$filename
            echo $line complete
            done < $FILE
            rm -f domains.txt
            break
            ;;
        "Удалить все домены")
            rm -rf /home/www/*
            rm -rf /etc/nginx/conf.d/*
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service nginx restart
            break
            ;;
        "Выход")
            break
            ;;
        *) echo invalid option;;
    esac
done