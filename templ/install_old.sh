#!/bin/bash
clear
generalLink="https://bitbucket.org/karachev/centos/raw/f95eeab7264296c58ed85842982ceb06e0abf292/templ/"
PS3='Настройка нового сервера.  Что делаем: '
options=("Настройка сервера" "Выход")
select opt in "${options[@]}"
do
 case $opt in
    "Настройка сервера")
            #utils
            apt-get update -y
            apt-get upgrade -y
            apt-get install --allow-unauthenticated software-properties-common -y
            apt-get install --allow-unauthenticated mc monit haproxy -y
            sed -i 's/ENABLED=0/ENABLED=1/' /etc/default/haproxy
            #reps
            add-apt-repository ppa:ondrej/php -y
            add-apt-repository ppa:ondrej/php5-compat -y
            apt-get update
            #nginx
            apt-get install --allow-unauthenticated -y nginx
            #php-fpm
            apt-get install --allow-unauthenticated -y php5.6 php5.6-cgi php5.6-cli php5.6-phpdbg php5.6-fpm php5.6-dev php5.6-common php5.6-curl php5.6-gd php5.6-imap php5.6-opcache php5.6-json php5.6-bz2 php5.6-mcrypt php5.6-xmlrpc php5.6-mbstring php5.6-xml php5.6-zip idn2
            cd /tmp
            wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
            tar xfz ioncube_loaders_lin_x86-64.tar.gz
            mv ioncube /usr/local/
            rm ioncube_loaders_lin_x86-64.tar.gz
            echo 'zend_extension = /usr/local/ioncube/ioncube_loader_lin_5.6.so' >> /etc/php/5.6/fpm/php.ini
            #tor
            apt-get install --allow-unauthenticated tor -y
            #configs
            #php
            wget $generalLink/i/www.conf -O /etc/php/5.6/fpm/pool.d/www.conf
            sed -i 's/cgi.fix_pathinfo=0/cgi.fix_pathinfo=1/' /etc/php/5.6/fpm/php.ini
            sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php/5.6/fpm/php.ini
            #nginx
            wget $generalLink/i/nginx.conf -O /etc/nginx/nginx.conf
            ip="$(ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
            wget $generalLink/i/root_domain -O /etc/nginx/conf.d/$ip.conf
            #monit
            wget $generalLink/i/nginx -O /etc/monit/conf.d/nginx
            wget $generalLink/i/haproxy -O /etc/monit/conf.d/haproxy
            wget $generalLink/i/php -O /etc/monit/conf.d/php
            wget $generalLink/i/tor -O /etc/monit/conf.d/tor
            #tor
            wget $generalLink/i/torrc -O /etc/tor/torrc
            wget $generalLink/i/torsocks.conf -O /etc/torsocks.conf
            #haproxy
            wget $generalLink/i/haproxy.cfg -O /etc/haproxy/haproxy.cfg
            # restart services
            /usr/sbin/service nginx restart
            /usr/sbin/service haproxy restart
            /usr/sbin/service tor restart
            /usr/sbin/service php5.6-fpm restart
            /usr/sbin/service monit restart
            /sbin/iptables -A INPUT -p tcp --dport 80 -j ACCEPT
            break
  ;;
 "Выход")
  break
  ;;
 *) echo invalid option;;
 esac
done