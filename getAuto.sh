#!/bin/bash
clear
repo="https://bitbucket.org/karachev/autobash/get/master.tar.gz"
PS3='Что делаем: '
options=("Обновление скриптов" "Выход")
select opt in "${options[@]}"
do
 case $opt in
    "Обновление скриптов")
    rm -r -f auto
    wget $repo
    tar xfz master.tar.gz
    mv *autobash* auto
    rm master.tar.gz
    break
    ;;
    "Выход")
    break
    ;;
 *) echo invalid option;;
 esac
done