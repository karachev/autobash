#!/bin/bash
clear
PS3="Что делаем: "
options=("Создать домен" "Создать домен ssl" "Создать домен c БД" "Создать домен c БД ssl" "Создать домен по шаблону" "Выход")
select opt in "${options[@]}"
do
    case $opt in
        "Создать домен")
            echo "Создать домен: "
            read domain
            
            cp auto/t/d/defaultNginxProxy.conf /etc/nginx/sites-enabled/$domain.conf
            cp auto/t/d/defaultApache.conf /etc/apache2/sites-enabled/$domain.conf
           
            sed -i "s/domainName/$domain/" /etc/nginx/sites-enabled/$domain.conf
            sed -i "s/www.domainName/www.$domain/" /etc/nginx/sites-enabled/$domain.conf
            
            sed -i "s/domainName/$domain/" /etc/apache2/sites-enabled/$domain.conf

            mkdir -p /var/www/$domain/{www,tmp}
            mkdir -p /var/www/$domain/log/{nginx,apache}
            echo "<h1>Hello from $domain</h1>" > /var/www/$domain/www/index.php
            chown -R www-data:www-data /var/www/$domain
            chmod -R 775 /var/www/$domain
            echo $domain complete
            systemctl reload nginx
            systemctl reload apache2
            echo "Домен $domain добавлен в систему"
            break
            ;;
                "Создать домен ssl")
            echo "Создать домен ssl: "
            read domain
            
            cp auto/t/d/defaultNginxProxy.conf /etc/nginx/sites-enabled/$domain.conf
            cp auto/t/d/defaultApache.conf /etc/apache2/sites-enabled/$domain.conf
           
            sed -i "s/domainName/$domain/" /etc/nginx/sites-enabled/$domain.conf
            sed -i "s/www.domainName/www.$domain/" /etc/nginx/sites-enabled/$domain.conf
            
            sed -i "s/domainName/$domain/" /etc/apache2/sites-enabled/$domain.conf

            mkdir -p /var/www/$domain/{www,tmp}
            mkdir -p /var/www/$domain/log/{nginx,apache}
            echo "<h1>Hello from $domain</h1>" > /var/www/$domain/www/index.php
            chown -R www-data:www-data /var/www/$domain
            chmod -R 775 /var/www/$domain
            echo $domain complete
            systemctl reload nginx
            systemctl reload apache2
            certbot --nginx -d $domain -d www.$domain
            echo "Домен $domain добавлен в систему"
            break
            ;;
        "Создать домен c БД")
            echo "Создать домен с БД: "
            read domain
            
            cp auto/t/d/defaultNginxProxy.conf /etc/nginx/sites-enabled/$domain.conf
            cp auto/t/d/defaultApache.conf /etc/apache2/sites-enabled/$domain.conf
           
            sed -i "s/domainName/$domain/" /etc/nginx/sites-enabled/$domain.conf
            sed -i "s/www.domainName/www.$domain/" /etc/nginx/sites-enabled/$domain.conf
            
            sed -i "s/domainName/$domain/" /etc/apache2/sites-enabled/$domain.conf

            mkdir -p /var/www/$domain/{www,tmp}
            mkdir -p /var/www/$domain/log/{nginx,apache}
            echo "<h1>Hello from $domain</h1>" > /var/www/$domain/www/index.php
            chown -R www-data:www-data /var/www/$domain
            chmod -R 775 /var/www/$domain
            echo $domain complete
            systemctl reload nginx
            systemctl reload apache2
            echo "Домен $domain добавлен в систему"
            # Создание базы данных
            pass=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c20)
            user=$(echo $domain | cut -f1 -d.)
            echo "$domain - $user - $pass" >> dbinfo.txt
            mysql -e "CREATE DATABASE $user DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"
            mysql -e "GRANT ALL PRIVILEGES ON $user.* TO $user@localhost IDENTIFIED BY '$pass' WITH GRANT OPTION"
            mysql -e "FLUSH PRIVILEGES;"
            echo "Создана база данных:"
            echo "Имя пользователя и БД - $user"
            echo "Пароль базы данных: $pass"
            break
            ;;
        "Создать домен c БД ssl")
            echo "Создать домен с БД ssl: "
            read domain
            
            cp auto/t/d/defaultNginxProxy.conf /etc/nginx/sites-enabled/$domain.conf
            cp auto/t/d/defaultApache.conf /etc/apache2/sites-enabled/$domain.conf
           
            sed -i "s/domainName/$domain/" /etc/nginx/sites-enabled/$domain.conf
            sed -i "s/www.domainName/www.$domain/" /etc/nginx/sites-enabled/$domain.conf
            
            sed -i "s/domainName/$domain/" /etc/apache2/sites-enabled/$domain.conf

            mkdir -p /var/www/$domain/{www,tmp}
            mkdir -p /var/www/$domain/log/{nginx,apache}
            echo "<h1>Hello from $domain</h1>" > /var/www/$domain/www/index.php
            chown -R www-data:www-data /var/www/$domain
            chmod -R 775 /var/www/$domain
            echo $domain complete
            systemctl reload nginx
            systemctl reload apache2
            certbot --nginx -d $domain -d www.$domain
            echo "Домен $domain добавлен в систему"
            # Создание базы данных
            pass=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c20)
            user=$(echo $domain | cut -f1 -d.)
            echo "$domain - $user - $pass" >> dbinfo.txt
            mysql -e "CREATE DATABASE $user DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"
            mysql -e "GRANT ALL PRIVILEGES ON $user.* TO $user@localhost IDENTIFIED BY '$pass' WITH GRANT OPTION"
            mysql -e "FLUSH PRIVILEGES;"
            echo "Создана база данных:"
            echo "Имя пользователя и БД - $user"
            echo "Пароль базы данных: $pass"
            break
            ;;
        "Создать домен по шаблону")
            echo "Не поддерживается!"
            break
            ;;
        "Выход")
            break
            ;;
        *) echo invalid option;;
    esac
done