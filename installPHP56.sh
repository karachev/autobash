#!/bin/bash
clear

#reps
add-apt-repository ppa:ondrej/php -y
add-apt-repository ppa:ondrej/php5-compat -y
apt-get update

# PHP
apt-get install --allow-unauthenticated -y php5.6-interbase php5.6-xmlrpc php5.6 php5.6-xml php5.6-xsl php5.6-zip php5.6-pspell php5.6-recode php5.6-tidy php5.6-pgsql php5.6-json php5.6-enchant php5.6-readline php5.6-curl php5.6-ldap php5.6-common php5.6-imap php5.6-bcmath php5.6-intl php5.6-gd php5.6-mbstring php5.6-odbc php5.6-mysql php5.6-mcrypt php5.6-snmp php5.6-soap php5.6-bz2 php5.6-cgi php5.6-cli php5.6-dba php5.6-dev php5.6-fpm php5.6-sqlite3 php5.6-sybase php5.6-gmp php5.6-opcache php5.6-phpdbg
systemctl enable php5.6-fpm
systemctl start php5.6-fpm

#Config PHP
sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php/5.6/fpm/php.ini
echo 'zend_extension = /usr/local/ioncube/ioncube_loader_lin_5.6.so' >> /etc/php/5.6/fpm/php.ini
echo 'zend_extension_ts = /usr/local/ioncube/ioncube_loader_lin_5.6_ts.so' >> /etc/php/5.6/fpm/php.ini
echo 'zend_extension = /usr/local/ioncube/ioncube_loader_lin_5.6.so' >> /etc/php/5.6/cli/php.ini
echo 'zend_extension_ts = /usr/local/ioncube/ioncube_loader_lin_5.6_ts.so' >> /etc/php/5.6/cli/php.ini

systemctl restart php5.6-fpm
systemctl restart apache2